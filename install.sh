#!/bin/bash

DESTDIR="$1"
PKG="$2"
SYSCONFDIR=/etc
PREFIX=/usr
SVDIR="${SYSCONFDIR}"/runit/sv
SVDDIR="${PREFIX}"/lib/rc/sv.d
RCDIR="${SYSCONFDIR}"/rc

install_rc() {
    if [ -d "${PKG}"/rc ]; then
        install -v -d "${DESTDIR}${RCDIR}"
        install -v -m644 "${PKG}"/rc/* "${DESTDIR}${RCDIR}"
    fi
    install -d "${DESTDIR}${SVDDIR}"
    install -v -m755 "${PKG}"/sv.d/* "${DESTDIR}${SVDDIR}"
}

install_sv() {
    install -d "${DESTDIR}${SVDIR}"

    for d in "${PKG}"/*; do
#         echo ${d#*/}
        if [ -d "$d"/log ];then
            install -v -d "${DESTDIR}${SVDIR}/${d#*/}/log"
            install -v -Dm644 "$d"/log/* "${DESTDIR}${SVDIR}/${d#*/}/log"

        fi
        install -v -Dm644 "$d"/* "${DESTDIR}${SVDIR}/${d#*/}"
    done
}

case "${PKG}" in
    lvm2|cryptsetup|apparmor|hdparm) install_rc ;;
    *) install_sv ;;
esac
